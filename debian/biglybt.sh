#!/bin/sh

java -cp /usr/share/java/biglybt-core.jar:/usr/share/java/biglybt-ui.jar:/usr/share/java/commons-cli.jar:/usr/share/java/swt4.jar:/usr/share/java/bcprov.jar \
     -Dazureus.install.path=/usr/share/biglybt \
     com.biglybt.ui.Main $@
